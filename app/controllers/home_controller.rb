class HomeController < ApplicationController

  before_action :authenticate_person!

  def index
    @email = current_person.email
  end
end
